import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {

  constructor(
    private http: HttpClient
  ) { }

  getHeaders(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return headers;
  }

  consultReservations(customerId: string){

    let url = environment.url + '/reservation?customerId=' + customerId;

    return new Promise((resolve, reject) => {

      this.http.get(url).subscribe((result) => {
        
        resolve(result);
      }, (error) => {
        
        reject(error);
      });
    });

  }

  insertReservation(reservation){
    
    let url = environment.url + '/reservation';

    return new Promise((resolve, reject) => {

      this.http.post(url, reservation, {
        headers: this.getHeaders()
      }).subscribe((response) => {
  
        resolve(response);
      }, (error) => {
  
        reject(error);
      });
    });
  }
}
