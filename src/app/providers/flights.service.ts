import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  flights = [];

  constructor(
    private http: HttpClient
  ) {
    this.getFlights();
  }

  getHeaders(){
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/JSON'
    });
    return headers;
  }

  getFlights(){

    let url = environment.url + '/flight';
    this.http.get(url,
      {
        headers: this.getHeaders()
      }).subscribe((result) => {
        
      if (result['ok']){
        this.flights = result['flights'];
      }
    }, (error) => {
      console.log(error);
    })
  }

  getFlight(idFlight: string){

    return this.flights.find((item) => {
      return item['_id'] == idFlight;
    });
  }
}
