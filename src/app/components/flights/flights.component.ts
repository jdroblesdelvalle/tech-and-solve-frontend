import { Component, OnInit } from '@angular/core';
import { FlightsService } from '../../providers/flights.service';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit {

  constructor(
    public _fs: FlightsService
  ) {
    _fs.getFlights();
  }

  ngOnInit() {
  }

}
