import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FlightsService } from '../../providers/flights.service';
import { ReservationsService } from '../../providers/reservations.service';
declare const $;

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  idFlight: string;
  reservation: any = {};

  modalTitle = '';
  modalBody = '';
  modalButton = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    public _fs: FlightsService,
    public _rs: ReservationsService,
    private router: Router
  ) {

    activatedRoute.params.subscribe((params) => {
      this.idFlight = params['id'];

    });
  }

  ngOnInit() {
  }

  submit(){
   
    if(!this.reservation['customerId'] || this.reservation['customerId'].toString().length < 7){
      return this.showModal("Error", "El número de identificación debe tener al menos 7 dígitos.");
    }

    let birthdate = new Date(this.reservation['birthdate']);
    birthdate.setFullYear(Number(birthdate.getFullYear()) + 18);
    if (birthdate > new Date()){
      return this.showModal("Error", "Usted debe ser mayor de edad para realizar una reservación.");
    }

    let flight = this._fs.getFlight(this.idFlight);
    if (flight){
      this.reservation.flight = this.idFlight;

      this._rs.insertReservation(this.reservation).then((result) => {

        if (result['ok']){
          this.showModal("Reservación", "Reservación exitosa", false);
          setTimeout(() => {
            this.hideModal();
            this.router.navigate(['/flights']);
          }, 3000);
        } else {
          this.showModal("Error", result['message']);
        }
      }).catch((error) => {

        this.showModal("Error", "Ocurrió un error, intentelo nuevamente.");
      });
    }
  }

  showModal(title, body, showButton = true){
    this.modalTitle = title;
    this.modalBody = body;
    this.modalButton = showButton;

    $('#exampleModal').modal({
      backdrop: 'static'
    });
  }

  hideModal(){

    $('#exampleModal').modal('hide');
  }

}
