import { Component, OnInit } from '@angular/core';
import { ReservationsService } from '../../providers/reservations.service';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  searchText: string;
  reservations;

  constructor(
    public _rs: ReservationsService
  ) { }

  ngOnInit() {
  }

  search(){
    
    this._rs.consultReservations(this.searchText).then((result) => {

      console.log(result);
      if(result['ok']){

        this.reservations = result['reservations'];
      } else {
        console.log("error");
      }
    }).catch((error) => {

      console.log(error);
    })
  }

}
