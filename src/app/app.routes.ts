import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { FlightsComponent } from './components/flights/flights.component';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { ReservationComponent } from './components/reservation/reservation.component';

export const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'flights', component: FlightsComponent },
    { path: 'reservations', component: ReservationsComponent },
    { path: 'reservation/:id', component: ReservationComponent },
    { path: '**', redirectTo: '', pathMatch: 'full'},
];

export const APP_ROUTES = RouterModule.forRoot(routes, {useHash: true});