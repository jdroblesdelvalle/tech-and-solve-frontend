import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { APP_ROUTES } from './app.routes';
import { FlightsComponent } from './components/flights/flights.component';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { HttpClientModule } from '@angular/common/http';
import { FlightsService } from './providers/flights.service';
import { ReservationsService } from './providers/reservations.service';
import { DatePipe } from './pipes/date.pipe';
import { TimePipe } from './pipes/time.pipe';
import { ReservationComponent } from './components/reservation/reservation.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FlightsComponent,
    ReservationsComponent,
    DatePipe,
    TimePipe,
    ReservationComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    FlightsService,
    ReservationsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
